Składniki:
1 i 1/2 szklanki mąki pszennej
1 i 1/2 łyżeczki proszku do pieczenia
szczypta soli
2 łyżeczki cukru pudru lub kryształu
1 łyżka cukru wanilinowego
2 jaja
1/2 szklanki oleju roślinnego (np. słonecznikowego) lub roztopionego masła
1 i 1/3 szklanki mleka
Wykonanie:
Mąkę wsypać do miski, dodać proszek do pieczenia, sól, cukier, cukier wanilinowy. Wszystko wymieszać a następnie dodać jajka, olej roślinny oraz mleko.
Zmiksować mikserem na gładką masę, tylko do połączenia się składników. Ciasto można odstawić aby odpoczęło (na około 15 minut), ale nie jest to konieczne.
Rozgrzać gofrownicę. Gofry piec przez około 3 - 3,5 minuty lub przez czas podany w instrukcji gofrownicy.
Nakładamy ciasto chochlą i wypukłą stroną łyżki rozprowadzamy ciasto dokładnie po całej powierzchni.
Gofry po upieczeniu odkładać na metalową kratkę.
Posypać cukrem pudrem i polać syropem klonowym. Lub podawać z ulubionymi dodatkami np. marmoladą, dżemem, owocami i bitą śmietaną.